package gamewisp

import (
	"fmt"
	"net/url"
)

type SubscribersResponse struct {
	Meta   Meta         `json:"meta"`
	Result Result       `json:"result"`
	Data   []Subscriber `json:"data"`
}

type Subscriber struct {
	ID     int64  `json:"id"`
	Status string `json:"status"`
}

func (client *GameWispClient) GetSubscribers(status string) (*SubscribersResponse, error) {
	var subscribers SubscribersResponse
	var e GameWispError

	v := &url.Values{}
	v.Add("status", status)

	resp, err := client.session.Get("https://api.gamewisp.com/pub/v1/channel/subscribers/", v, &subscribers, &e)
	if err != nil {
		return nil, err
	}

	if resp.Status() != 200 {
		return nil, fmt.Errorf("Status: %d, Message: %s", resp.Status(), e.ErrorDescription)
	}

	return &subscribers, nil
}
