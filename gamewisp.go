package gamewisp

import (
	"net/http"

	"gopkg.in/jmcvetta/napping.v3"
)

type GameWispClient struct {
	session napping.Session
}

func NewGameWispClient(httpClient *http.Client) GameWispClient {
	return GameWispClient{
		session: napping.Session{
			Client: httpClient,
		},
	}
}

type GameWispError struct {
	Error            string `json:"error"`
	ErrorDescription string `json:"error_description"`
}

type Meta struct {
	Cursor Cursor `json:"cursor"`
}

type Cursor struct {
	Count int64 `json:"count"`
}

type Result struct {
	Status  int64  `json:"status"`
	Message string `json:"message"`
}
