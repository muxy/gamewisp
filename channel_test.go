package gamewisp_test

import (
	"encoding/json"
	"testing"

	"bitbucket.org/muxy/gamewisp"
)

func TestDecodeExample(t *testing.T) {
	exampleData := `
{
  "result": {
    "status": 1,
    "message": "Subscribers Retrieved"
  },
  "data": [
    {
      "id": 26,
      "user_id": 87545,
      "status": "active",
      "subscribed_at": null,
      "tier_id": "3",
      "user": {
        "data": {
          "id": 87545,
          "username": "test",
          "banned": false,
          "deactivated": false,
          "created_at": "2015-11-10 00:00:00",
          "links": {
            "uri": "/user/87545"
          }
        }
      }
    }
  ],
  "meta": {
    "cursor": {
      "current": null,
      "prev": null,
      "next": "MjY=",
      "count": 1
    }
  }
}
`

	var response gamewisp.SubscribersResponse
	err := json.Unmarshal([]byte(exampleData), &response)
	if err != nil {
		t.Error(err)
	}

}
