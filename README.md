# Gamewisp API Client for GO

[![Build Status](https://drone.io/bitbucket.org/muxy/gamewisp/status.png)](https://drone.io/bitbucket.org/muxy/gamewisp/latest)
[![MIT](http://img.shields.io/badge/license-MIT-green.svg)](LICENSE)
